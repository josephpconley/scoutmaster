var alertHideInterval = 2000;

function error(msg){
    if(msg){
        $('#flash').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">x</button><strong>' + msg + '</strong></div>');
        setTimeout(function(){
            $(".alert").alert('close')
        }, alertHideInterval)
    }
}

function success(msg){
    if(msg){
        $('#flash').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button><strong id="successMsg">' + msg + '</strong></div>');
        setTimeout(function(){
            $(".alert").alert('close')
        }, alertHideInterval)
    }
}

function sum(arr){
    var sum = 0;
    for(var i = 0; i < arr.length; i++){
        sum += arr[i]
    }
    return sum;
}

function avg(arr){
    return sum(arr)/arr.length;
}

function closeModal(){
    $('.modal').modal("hide")
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

function compare(o1, o2){
    return o1 == o2 ? 0 : (o1 < o2 ? -1 : 1)
}

function parseHTML(text) {
    if(text){
        var re = /(\(.*?)?\b((?:https?|ftp|file):\/\/[-a-z0-9+&@#\/%?=~_()|!:,.;]*[-a-z0-9+&@#\/%=~_()|])/ig;
        return text.replace(re, function(match, lParens, url) {
            var rParens = '';
            lParens = lParens || '';

            // Try to strip the same number of right parens from url
            // as there are left parens.  Here, lParenCounter must be
            // a RegExp object.  You cannot use a literal
            //     while (/\(/g.exec(lParens)) { ... }
            // because an object is needed to store the lastIndex state.
            var lParenCounter = /\(/g;
            while (lParenCounter.exec(lParens)) {
                var m;
                // We want m[1] to be greedy, unless a period precedes the
                // right parenthesis.  These tests cannot be simplified as
                //     /(.*)(\.?\).*)/.exec(url)
                // because if (.*) is greedy then \.? never gets a chance.
                if (m = /(.*)(\.\).*)/.exec(url) ||
                        /(.*)(\).*)/.exec(url)) {
                    url = m[1];
                    rParens = m[2] + rParens;
                }
            }
            return lParens + "<a target='_blank' href='" + url + "'>" + url + "</a>" + rParens;
        });
    }
    return text
}