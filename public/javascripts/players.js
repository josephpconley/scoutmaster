function parseItem(doc){
    var item;
    if(doc.itemId == "article"){
        item = new Article(doc.item)
    } else if(doc.itemId == "tweet"){
        item = new Tweet(doc.item)
    } else if(doc.itemId == "video"){
        item = new Video(doc.item)
    } else if(doc.itemId == "note"){
        item = new Note(doc.item)
    }

    if(item){
        item.load(doc)
        return item;
    }
    return null;
}

function Tag(data){
    var self = this
    self.value = ko.observable(0)
    self.text = ko.observable()

    if(data){
        self.value(data.value)
        self.text(data.text)
    }

    self.setTagValue = function(v){
        self.value(v)
    }

    //for follow/unfollow
    self.hover = ko.observable(false)
    self.hoverText = ko.computed(function(){
        if(self.hover()){
            return "Unfollow"
        }
        return "Following"
    })
    self.toggleHover = function(){
        self.hover(!self.hover())
    }
}

function Item(id, item){
    var self = this;
    self._id = ko.observable()
    self.itemId = ko.observable(id);
    self.item = ko.observable()
    if(item){
        if(typeof item == "string"){
            self.item(item)
        }else{
            self.item(ko.mapping.fromJS(item));
        }
    }
    self.playerId = ko.observable();
    self.notes = ko.observable('');
    self.tags = ko.observableArray([]);
    self.tagText = ko.observable()

    self.createdBy = ko.observable()
    self.updateDate = ko.observable()
    self.updateDateStr = ko.computed(function(){
        return moment(self.updateDate()).fromNow()
    })

    self.load = function(data){
        self._id(data._id)
        self.itemId(data.itemId)
        self.playerId(data.playerId)
        self.notes(data.notes)
        self.updateDate(new Date(data.updateDate))
        self.createdBy(data.createdBy)
        if(data.tags){
            for(var i = 0; i < data.tags.length; i++){
                self.tags.push(new Tag(data.tags[i]))
            }
        }
    }

    self.createTag = function(data, event){
        if(event.keyCode === 13){
            self.tags.push(new Tag({text: self.tagText(), value: 0}))
            self.tagText('');
            return false;
        }
        return true;
    }

    self.removeTag = function(tag){
        self.tags.remove(tag)
    }

    self.js = function(){
        var js = ko.toJS(self)
        delete js['__ko_mapping__'];
        return js;
    }

    self.showButton = ko.observable(false)
    self.toggleButton = function(){
        self.showButton(!self.showButton())
    }
}

function Note(data){
    var self = this;
    ko.utils.extend(self, new Item("note", data));
    ko.mapping.fromJS(data, {}, self);

    self.body = ko.computed(function(){
        return parseHTML(self.item())
    })
}

function Article(data){
    var self = this;
    ko.utils.extend(self, new Item("article", data));
    ko.mapping.fromJS(data, {}, self);
}

function Video(data){
    var self = this;
    ko.mapping.fromJS(data, {}, self);
    ko.utils.extend(self, new Item("video", data));

    self.created = ko.computed(function(){
        return moment(Date.parse(self.snippet.publishedAt())).fromNow()
    })

    self.videoUrl = ko.computed(function(){
        return 'http://www.youtube.com/embed/' + self.id.videoId() + '?autoplay=0';
    })
}

function Tweet(data){
    var self = this;
    ko.utils.extend(self, new Item("tweet", data));
    ko.mapping.fromJS(data, {}, self);

    self.created = ko.computed(function(){
        return moment(self.created_at(), 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').fromNow();
    })

    self.userUrl = ko.computed(function(){
        return "https://twitter.com/" + self.user.screen_name()
    })

    self.body = ko.computed(function(){
        var body = self.text();
        for(var i = 0; i < self.entities.urls().length; i++){
            var url = self.entities.urls()[i];
            body = body.replace(url.url(), '<a target="_blank" href="' + url.url() + '">' + url.display_url() + "</a>")
        }
        for(var i = 0; i < self.entities.hashtags().length; i++){
            var hashtag = self.entities.hashtags()[i];
            body = body.replace("#" + hashtag.text(), '<a target="_blank" href="https://twitter.com/hashtag/' + hashtag.text() + '">#' + hashtag.text() + "</a>")
        }
        for(var i = 0; i < self.entities.user_mentions().length; i++){
            var mention = self.entities.user_mentions()[i];
            body = body.replace("@" + mention.screen_name(), '<a target="_blank" href="https://twitter.com/' + mention.screen_name() + '">@' + mention.screen_name() + "</a>")
        }
        return body;
    })
}

function Player(data){
    var self = this;
    ko.mapping.fromJS(data, {}, self);

    self.loading = ko.observable(false);
    self.items = ko.observableArray([]);

    self.loadItems = function(id){
        self.loading(true)
        routes.controllers.Players.items(id).ajax({
            success: function(data){
                for(var i = 0; i < data.length; i++){
                    var item = parseItem(data[i]);
                    if(item){
                        self.items.push(item)
                    }
                }
                self.loading(false)
                self.chart();
            },
            error: function(data){
                console.log(data)
                self.loading(false)
            }
        })
    }

    self.tagMap = ko.computed(function(){
        var tagMap = {};
        for(var i = 0; i < self.items().length; i++){
            for(var j = 0; j < self.items()[i].tags().length; j++){
                var tag = self.items()[i].tags()[j]
                if(tag.text() in tagMap){
                    tagMap[tag.text()].push(tag.value());
                }else{
                    tagMap[tag.text()] = [tag.value()];
                }
            }
        }

        var tagArray = []
        for(var tag in tagMap){
            tagArray.push(new Tag({text: tag, value: avg(tagMap[tag])}))
        }
        return tagArray.sort(function(a, b){ return compare(b.value(), a.value())})
    })

    self.chart = function(){
        var ticks = [];
        var rawData = [];
        var tagMap = self.tagMap();
        for(var i = 0; i < tagMap.length; i++){
            var tag = tagMap[i];
            if(tag.value() > 0){
                rawData.push([tag.value(), i]);
                ticks.push([i, tag.text()]);
            }
        }

        var dataSet = [{ label: "Ratings", data: rawData, color: "#E8E800" }];
        var options = {
            series: {
                bars: {
                    show: true
                }
            },
            bars: {
                align: "center",
                barWidth: 0.5,
                horizontal: true,
                fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1}] },
                lineWidth: 0.5
            },
            xaxis: {
                max: 5,
                tickColor: "#5E5E5E",
                color: "black"
            },
            yaxis: {
                tickColor: "#5E5E5E",
                ticks: ticks,
                color: "black"
            },
            grid: {
                hoverable: true,
                borderWidth: 2
            }
        };

        $.plot($("#flot"), dataSet, options);
    }

    self.items.subscribe(function(arr){
        self.chart();
    })

    self.imageUrl = ko.computed(function(){
        return 'http://a.espncdn.com/i/nba/draft2015/headshots/' + self.espnId() + '.jpg';
    });

    self.name = ko.computed(function(){
        return self.firstName() + " " + self.lastName();
    })

    self.hw = ko.computed(function(){
        return parseInt(self.height() / 12) + "' " + self.height() % 12 + "'' " + self.weight() + "lbs."
    })

    //for follow/unfollow
    self.hover = ko.observable(false)
    self.hoverText = ko.computed(function(){
        if(self.hover()){
            return "Unfollow"
        }
        return "Following"
    })
    self.toggleHover = function(){
        self.hover(!self.hover())
    }
}

function Filter(){
    var self = this;
    self.pg = ko.observable(true)
    self.sg = ko.observable(true)
    self.sf = ko.observable(true)
    self.pf = ko.observable(true)
    self.c = ko.observable(true)

    self.positions = ko.computed(function(){
        var positions = []
        if(self.pg()){
            positions.push("PG")
        }
        if(self.sg()){
            positions.push("SG")
        }
        if(self.sf()){
            positions.push("SF")
        }
        if(self.pf()){
            positions.push("PF")
        }
        if(self.c()){
            positions.push("C")
        }
        return positions;
    })

    self.tags = ko.observableArray([])
}

function PlayerList(data, selectedPlayerId){
    var self = this;
    ko.utils.extend(self, new Social());

    self.allPlayers = ko.observableArray();
    self.players = ko.observableArray();
    self.filter = ko.observable(new Filter())

    self.filter().positions.subscribe(function(positions){
        var players = ko.utils.arrayFilter(self.allPlayers(), function(player) {
          return positions.indexOf(player.position()) > -1
        });
        self.players(players)
        if(players[0].id() != self.selectedPlayer().id()){
            self.selectedPlayer(players[0])
        }
    })

    self.selectedPlayer = ko.observable();
    self.togglePlayer = function(player){
        self.selectedPlayer(player);
    }

    self.apis = ko.observableArray(["youtube", "google", "twitter"])
    self.selectedApi = ko.observable(self.apis()[0])
    self.toggleApi = function(api){
        self.selectedApi(api);
    }

    self.searchTerm = ko.observable()
    self.tweets = ko.observableArray();
    self.videos = ko.observableArray();
    self.articles = ko.observableArray();
    self.newNote = ko.observable(new Note())

    self.loading = ko.observable(false);
    self.selectedApi.subscribe(function(){
        self.search();
    })

    self.selectedPlayer.subscribe(function(s){
        if(s.items().length == 0){
            self.selectedPlayer().loadItems(s.id());
        }else{
            s.chart();
        }
        self.searchTerm(s.name());
        self.search();
    })

    self.findItemsById = function(id){
        if(id == "google"){
            return self.articles();
        } else if(id == "twitter"){
            return self.tweets();
        } else if(id == "youtube"){
            return self.videos();
        }
        return null;
    }

    self.enterSearch = function(data, event){
        if(event.keyCode === 13){
            self.search()
            return false;
        }
        return true;
    }

    self.search = function(){
        self.tweets.removeAll();
        self.videos.removeAll();
        self.articles.removeAll();
        var items = self.findItemsById(self.selectedApi());
        self.loading(true);
        routes.controllers.Players.search(self.selectedPlayer().id(), self.selectedApi(), self.searchTerm()).ajax({
            success: function(data){
                if(self.selectedApi() == "twitter"){
                    for(var i = 0; i < data.statuses.length; i++){
                        self.tweets.push(new Tweet(data.statuses[i]))
                    }
                } else if(self.selectedApi() == "youtube"){
                    for(var i = 0; i < data.items.length; i++){
                        self.videos.push(new Video(data.items[i]))
                    }
                } else if(self.selectedApi() == "google"){
                    for(var i = 0; i < data.items.length; i++){
                        self.articles.push(new Article(data.items[i]))
                    }
                }
                self.loading(false);
            },
            error: function(data){
                console.log(data);
                self.loading(false);
            }
        })
    }

    self.save = function(item){
        var js = item.js();
        js["playerId"] = self.selectedPlayer().id();
        routes.controllers.Players.save().ajax({
            data: ko.toJSON(js),
            contentType: 'text/json',
            success: function(data){
                if(!item._id()){
                    self.selectedPlayer().items.splice(0, 0, parseItem(data));
                    if(item.itemId() == 'article'){
                        self.articles.remove(item);
                    } else if(item.itemId() == 'tweet'){
                        self.tweets.remove(item);
                    } else if(item.itemId() == 'video'){
                        self.videos.remove(item);
                    }
                }
                closeModal();
            },
            error: function(data){
                console.log(data);
            }
        })
    }

    if(data){
        for(var i = 0; i < data.length; i++){
            var player = new Player(data[i]);
            self.players.push(player);
            self.allPlayers.push(player);
        }
        if(selectedPlayerId){
            self.selectedPlayer(
                ko.utils.arrayFirst(self.allPlayers(), function(player) {
                    return player.id() == selectedPlayerId
                })
            )
        }else{
            self.selectedPlayer(self.players()[0]);
        }
    }
}