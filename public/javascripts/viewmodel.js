ko.bindingHandlers.numericValue = {
   init : function(element, valueAccessor, allBindingsAccessor) {
       var underlyingObservable = valueAccessor();
       var interceptor = ko.dependentObservable({
           read: underlyingObservable,
           write: function(value) {
               if (!isNaN(value)) {
                   underlyingObservable(parseFloat(value));
               }
           }
       });
       ko.bindingHandlers.value.init(element, function() { return interceptor }, allBindingsAccessor);
   },
   update : ko.bindingHandlers.value.update
};

ko.bindingHandlers.stringEmptyNull = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var underlyingObservable = valueAccessor();
        var interceptor = ko.dependentObservable({
            read: underlyingObservable,
            write: function (value) {
                if (value != null && value.trim() == '')
                    underlyingObservable(null);
                else
                    underlyingObservable(value);
            }
        });
        ko.bindingHandlers.value.init(element, function () { return interceptor }, allBindingsAccessor);
    },
    update: ko.bindingHandlers.value.update
};

function Social(){
    var self = this;
    self.followedPlayers = ko.observableArray([]);
    routes.controllers.Users.playerIds().ajax({
        success: function(data){
            for(var i = 0; i < data.length; i++){
                self.followedPlayers.push(data[i])
            }
        },
        error: function(data){
            console.log(data)
        }
    })
    self.followPlayer = function(player){
        routes.controllers.Players.follow(player.id()).ajax({
            success: function(data){
                self.followedPlayers.push(player.id())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
    self.unfollowPlayer = function(player){
        routes.controllers.Players.unfollow(player.id()).ajax({
            success: function(data){
                self.followedPlayers.remove(player.id())
            },
            error: function(data){
                console.log(data)
            }
        })
    }

    self.followedTags = ko.observableArray([]);
    routes.controllers.Users.tagIds().ajax({
        success: function(data){
            for(var i = 0; i < data.length; i++){
                self.followedTags.push(data[i])
            }
        },
        error: function(data){
            console.log(data)
        }
    })
    self.followTag = function(t){
        routes.controllers.Tags.follow(t.text()).ajax({
            success: function(data){
                self.followedTags.push(t.text())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
    self.unfollowTag = function(t){
        routes.controllers.Tags.unfollow(t.text()).ajax({
            success: function(data){
                self.followedTags.remove(t.text())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
}