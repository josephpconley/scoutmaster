function Leader(data){
    var self = this;
    self.player = ko.observable(new Player(data.player))
    self.tagCount = ko.observable(data.tagCount)
}

function TagList(data, selectedTagName){
    var self = this;
    self.tags = ko.observableArray();

    self.selectedTag = ko.observable();
    self.toggleTag = function(tag){
        self.selectedTag(tag);
    }

    self.items = ko.observableArray([]);
    self.leaders = ko.observableArray([]);

    self.newNote = ko.observable(new Note())
    self.loading = ko.observable(false);
    self.selectedTag.subscribe(function(s){
        self.loadItems();
        self.loadLeaders();
    })

    self.loadItems = function(){
        self.loading(true)
        self.items.removeAll()
        routes.controllers.Tags.items(self.selectedTag().text()).ajax({
            success: function(data){
                for(var i = 0; i < data.length; i++){
                    var item = parseItem(data[i]);
                    if(item){
                        self.items.push(item)
                    }
                }
                self.loading(false)
            },
            error: function(data){
                console.log(data)
                self.loading(false)
            }
        })
    }

    self.loadLeaders = function(){
        self.leaders.removeAll();
        routes.controllers.Players.leaders(self.selectedTag().text()).ajax({
            success: function(data){
                for(var i = 0; i < data.length; i++){
                    self.leaders.push(new Leader(data[i]))
                }
            },
            error: function(data){
                console.log(data)
            }
        })
    }

    if(data){
        for(var i = 0; i < data.length; i++){
            self.tags.push(new Tag(data[i]));
        }
        if(selectedTagName){
            self.selectedTag(
                ko.utils.arrayFirst(self.tags(), function(tag) {
                    return tag.text() == selectedTagName
                })
            )
        }else{
            self.selectedTag(self.tags()[0]);
        }
    }

    self.followedPlayers = ko.observableArray([]);
    routes.controllers.Users.playerIds().ajax({
        success: function(data){
            for(var i = 0; i < data.length; i++){
                self.followedPlayers.push(data[i])
            }
        },
        error: function(data){
            console.log(data)
        }
    })
    self.followPlayer = function(player){
        routes.controllers.Players.follow(player.id()).ajax({
            success: function(data){
                self.followedPlayers.push(player.id())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
    self.unfollowPlayer = function(player){
        routes.controllers.Players.unfollow(player.id()).ajax({
            success: function(data){
                self.followedPlayers.remove(player.id())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
    self.followedTags = ko.observableArray([]);
    routes.controllers.Users.tagIds().ajax({
        success: function(data){
            for(var i = 0; i < data.length; i++){
                self.followedTags.push(data[i])
            }
        },
        error: function(data){
            console.log(data)
        }
    })
    self.followTag = function(t){
        routes.controllers.Tags.follow(t.text()).ajax({
            success: function(data){
                self.followedTags.push(t.text())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
    self.unfollowTag = function(t){
        routes.controllers.Tags.unfollow(t.text()).ajax({
            success: function(data){
                self.followedTags.remove(t.text())
            },
            error: function(data){
                console.log(data)
            }
        })
    }
}