#ScoutMaster

##Run Locally
To build/run locally, you'll need the following items installed
* Play Framework v2.3.2 OR recent version of sbt
* Scala
* PostgreSQL 9.1 or higher (see application.conf for database name/creds)
* MongoDB

##Build/Deploy
Run `play dist` or `sbt dist` to build the distribution zip file.  Use the deploy.bat script to deploy to OpenShift