import java.sql.Timestamp

import org.joda.time.DateTime
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._

import models._

/*
* Helpers for common Slick operations
 */
package object extensions {

  trait HasId {
    val id: Long
  }

  trait HasTableId { _:Table[_]	=>
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  }

  implicit def time2dateTime = MappedColumnType.base[DateTime, Timestamp] (
    dateTime => new Timestamp(dateTime.getMillis),
    time => new DateTime(time.getTime)
  )

  trait HasDates {
    val createDate: DateTime
    val updateDate: DateTime
  }

  trait HasTableDates { _:Table[_]	=>
    def createDate = column[DateTime]("create_date")
    def updateDate = column[DateTime]("update_date")
  }

  implicit class IdExtensions[T <: HasTableId, C] (val	q: Query[T, C]){
    def findById(id: Long): Option[C] = DB withSession { implicit session =>
      q.where(_.id === id).firstOption
    }
  }

  implicit class DateExtensions[T <: HasTableDates, C] (val	q: Query[T, C]){
    def findRecent: List[C] = DB withSession { implicit session =>
      q.sortBy(_.updateDate desc).list()
    }
  }

  implicit class QueryExtensions2[T,E] (val q: Query[T, E]){
    def autoJoin[T2, E2](	q2: Query[T2, E2])( implicit condition:	(T,T2) => Column[Boolean]):	Query[(T,T2),(E,E2)] = q.join(q2).on(condition)
  }

  trait Crud[C <: HasId, T <: Table[C] with HasTableId] {
    val q: TableQuery[T]

    def count: Int = DB withSession { implicit session =>
      q.length.run
    }

    def max: Long = DB withSession { implicit session =>
      q.map(_.id).max.run.getOrElse(0)
    }

    def findById(id: Long): Option[C] = q.findById(id)

    def list[A](implicit request: DBSessionRequest[A]): List[C] = q.list()(request.dbSession)

    def insert(obj: C): Long = DB withSession { implicit session =>
      (q returning q.map(_.id)) += obj
    }

    def update(obj: C, id: Long): Long = DB withSession { implicit session =>
      q.where(_.id === id).update(obj)
      id
    }

    def update(obj: C): Long = update(obj, obj.id)

    def save(obj: C): Long = if(obj.id > 0) update(obj, obj.id) else insert(obj)

    def delete(id: Long) = DB withSession { implicit session =>
      q.where(_.id === id).delete
    }
  }

  implicit class JoinExtensions[T,E](val q:	Query[T,E]){
    def autoJoin[T2, E2](q2: Query[T2, E2])(implicit condition:(T, T2) => Column[Boolean]): Query[(T,T2),(E,E2)] = q.join(q2).on(condition)
  }

  object Models{
    def ddl = DB withSession { implicit session =>
      PlayerTable.q.ddl ++ PlayerFollowersTable.q.ddl ++ TagFollowersTable.q.ddl ++ OAuthBearerTokenTable.q.ddl
    }
  }
}
