package apis

import apis.Google._
import controllers.Application.Authenticated
import models.OAuthBearerTokenTable
import play.api.Play.current
import play.api.libs.json._
import play.api.libs.ws.WS
import play.api.mvc._
import sun.misc.BASE64Encoder

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Twitter extends API with Controller{

  val id = "twitter"
  val itemId = "tweet"
  val key = config.getString("twitter.key").get
  val secret = config.getString("twitter.secret").get
  val base64 = (key + ":" + secret).getBytes
  val bearerToken = new BASE64Encoder().encode(base64).replaceAll("\n", "").replaceAll("\r", "")

  def idReader(js: JsValue): String = (js \ "id_str").as[String]

  def authenticate = Action.async { implicit req =>
    WS.url("https://api.twitter.com/oauth2/token")
      .withHeaders("Authorization" -> ("Basic " + bearerToken), "Content-Type" -> "application/x-www-form-urlencoded;charset=UTF-8")
      .post("grant_type=client_credentials") map { res =>

      val token = (res.json \ "access_token").as[String]
      OAuthBearerTokenTable.save("twitter", token)
      Ok.withSession("twitter_token" -> token)
    }
  }

  def search(name: String, excludeIds: Seq[String])(implicit req: RequestHeader): Future[Result] = {
    val bToken = req.session.get("twitter_token") orElse OAuthBearerTokenTable.findById(id).map(_.token)
    bToken map { token =>
      WS.url("https://api.twitter.com/1.1/search/tweets.json")
        .withQueryString("q" -> name)
        .withHeaders("Authorization" -> s"Bearer $token")
        .get() map { res =>

        val items = (res.json.as[JsObject] \ "statuses").as[JsArray].value.filter(item => !excludeIds.contains(idReader(item)))
        Ok(res.json.as[JsObject] ++ Json.obj("items" -> items)).withSession("twitter_token" -> token, "user" -> req.session.get("user").get)
      }
    } getOrElse Future.successful(BadRequest)
  }
}