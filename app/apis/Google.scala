package apis

import play.api.libs.json._
import play.api.libs.ws.WS
import play.api.mvc._

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Google extends API with Controller {

  val id = "google"
  val itemId = "article"
  val key = config.getString("google.key").get
  val engineId = "011417049639741320360:subo4ppzha8"

  def idReader(js: JsValue): String = (js \ "cacheId").as[String]

  def search(name: String, excludeIds: Seq[String])(implicit req: RequestHeader): Future[Result] = {
    WS.url("https://www.googleapis.com/customsearch/v1")
      .withQueryString("q" -> name, "cx" -> engineId, "key" -> key)
      .get() map { res =>
      val items = (res.json.as[JsObject] \ "items").as[JsArray].value.filter(item => !excludeIds.contains(idReader(item)))
      Ok(res.json.as[JsObject] ++ Json.obj("items" -> items))
    }
  }
}