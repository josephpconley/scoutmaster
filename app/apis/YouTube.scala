package apis

import play.api.libs.json._
import play.api.libs.ws.WS
import play.api.mvc._

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object YouTube extends API with Controller {

  val id = "youtube"
  val itemId = "video"
  val key = config.getString("google.key").get

  def idReader(js: JsValue): String = (js \ "id" \ "videoId").as[String]

  def search(name: String, excludeIds: Seq[String])(implicit req: RequestHeader): Future[Result] = {
    WS.url("https://www.googleapis.com/youtube/v3/search")
      .withQueryString("q" -> name, "part" -> "snippet", "maxResults" -> "20", "key" -> key)
      .get() map { res =>

      val items = (res.json.as[JsObject] \ "items").as[JsArray].value.filter { item =>
        (item \ "id" \ "videoId").asOpt[String].isDefined && !excludeIds.contains(idReader(item))
      }
      Ok(res.json.as[JsObject] ++ Json.obj("items" -> items))
    }
  }
}