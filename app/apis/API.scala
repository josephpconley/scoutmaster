package apis

import play.api.libs.json.{JsValue, Reads, JsString}
import play.api.mvc.{RequestHeader, Result}

import scala.concurrent.Future

trait API {
  val id: String
  val itemId: String
  val key: String
  val config = play.api.Play.current.configuration

  def idReader(js: JsValue): String

  def search(name: String, excludeIds: Seq[String])(implicit req: RequestHeader): Future[Result]
}

object API {
  lazy val list: Seq[API] = Seq(Twitter, YouTube, Google)
}