package notifications

import com.typesafe.plugin._
import models.{UserNotificationsTable, PlayerTable, Player}
import play.api.Play.current
import play.api.libs.json.{JsArray, JsObject}

object Email {
  def newPlayerPost(post: JsObject) = {
    val player = PlayerTable.findById( (post \ "playerId").as[Long]).get
    val itemId = (post \ "itemId").as[String]
    val tags = (post \ "tags").as[JsArray].value.map(t => (t \ "text").as[String])

    val mail = use[MailerPlugin].email
    mail.setSubject(s"New Post for ${player.firstName} ${player.lastName}")
    mail.setFrom("notifications@scoutmaster.com")

    val recipients = UserNotificationsTable.findFollowers(player.id)
    mail.setRecipient(recipients:_*)

    val body = views.html.emails.newPlayerPost(player, itemId, tags).toString()
    mail.sendHtml(body)
  }
}

