package messages

import controllers.Players.{Tag}
import models._
import play.api.libs.json.Json

trait JSONFormats {
  implicit lazy val playerFmt = Json.format[Player]
  implicit lazy val oauthFmt = Json.format[OAuthToken]
  implicit lazy val tagFmt = Json.format[Tag]
}
