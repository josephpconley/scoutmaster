package models

import extensions._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import org.joda.time.DateTime
import play.api.libs.oauth.RequestToken

case class OAuthBearerToken(id: String, token: String, expires: DateTime)

class OAuthBearerTokenTable(tag: Tag) extends Table[OAuthBearerToken](tag, "oauth_bearer_token") {
  def id = column[String]("id", O.PrimaryKey)
  def token = column[String]("bearer_token")
  def expires = column[DateTime]("expires")
  def * = (id, token, expires) <> ( OAuthBearerToken.tupled, OAuthBearerToken.unapply)
}

object OAuthBearerTokenTable {
  val q = TableQuery[OAuthBearerTokenTable]

  def findById(id: String): Option[OAuthBearerToken] = DB withSession { implicit session =>
    q.where(_.id === id).firstOption
  }

  def save(id: String, token: String) = DB withSession { implicit session =>
    q.where(_.id === id).delete
    q.insert(OAuthBearerToken(id, token, DateTime.now()))
  }
}

case class OAuthToken(id: String, token: String, secret: String, expires: DateTime){
  lazy val pair = RequestToken(token, secret)
}

class OAuthTokenTable(tag: Tag) extends Table[OAuthToken](tag, "oauth_token") {
  def id = column[String]("id", O.PrimaryKey)
  def token = column[String]("token")
  def secret = column[String]("secret")
  def expires = column[DateTime]("expires")
  def * = (id, token, secret, expires) <> ( OAuthToken.tupled, OAuthToken.unapply)
}

object OAuthTokenTable {
  val q = TableQuery[OAuthTokenTable]

  def findById(id: String): Option[OAuthToken] = DB withSession { implicit session =>
    q.where(_.id === id).firstOption
  }

  def save(id: String, token: String, secret: String) = DB withSession { implicit session =>
    q.where(_.id === id).delete
    q.insert(OAuthToken(id, token, secret, DateTime.now()))
  }
}