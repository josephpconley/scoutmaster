package models

import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import play.api.db.slick._

class UserNotificationsTable(tag: Tag) extends Table[(String, String)](tag, "user_notifications"){
  def user = column[String]("user")
  def email = column[String]("email")
  def * = (user, email)
  def pk = primaryKey("user_notifications_pk", user -> email)
}

object UserNotificationsTable {
  val q = TableQuery[UserNotificationsTable]

  def save(user: String, email: String) = DB withSession { implicit session =>
    q.where(_.user === user).delete
    q.insert(user -> email)
  }

  def findByUser(user: String) = DB withSession { implicit session =>
    q.where(_.user === user).map(_.email).firstOption
  }

  def findFollowers(pid: Long): Seq[String] = DB withSession { implicit session =>
    (for {
      followers <- PlayerFollowersTable.q
      emails <- q if followers.user === emails.user
    } yield emails.email).list()
  }
}