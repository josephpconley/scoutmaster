package models

import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import play.api.db.slick._

class TagFollowersTable(tag: Tag) extends Table[(String, String)](tag, "tag_followers"){
  def tagName = column[String]("tag")
  def user = column[String]("user")
  def * = (tagName, user)
  def pk = primaryKey("tag_followers_pk", tagName -> user)
}

object TagFollowersTable {
  val q = TableQuery[TagFollowersTable]

  def follow(tagName: String, user: String) = DB withSession { implicit session =>
    q.insert(tagName -> user)
  }

  def unfollow(tagName: String, user: String) = DB withSession { implicit session =>
    q.where(_.tagName === tagName).where(_.user === user).delete
  }

  def findByUser(user: String): Seq[String] = DB withSession { implicit session =>
    q.where(_.user === user).map(_.tagName).list()
  }
}
