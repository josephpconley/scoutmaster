package models

import extensions._
import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import play.api.db.slick._

case class Player(firstName: String, lastName: String, age: Int, height: Double, weight: Double, position: String,
                    school: String, espnId: String, id: Long = -1) extends HasId

class PlayerTable(tag: Tag) extends Table[Player](tag, "player") with HasTableId{
  def firstName = column[String]("first_name")
  def lastName = column[String]("last_name")
  def age = column[Int]("age")
  def height = column[Double]("height")
  def weight = column[Double]("weight")

  //TODO figure out how to map this to a Postgres array
  def position = column[String]("position")
  def school = column[String]("school")
  def espnId = column[String]("espn_id")
  def * = (firstName, lastName, age, height, weight, position, school, espnId, id) <> ( Player.tupled, Player.unapply)
}

object PlayerTable extends Crud[Player, PlayerTable] {
  val q = TableQuery[PlayerTable]

  def find = DB withSession { implicit session =>
    q.list()
  }

  def findByIds(ids: Seq[Long]): List[Player] = DB withSession { implicit session =>
    q.where(_.id inSet ids).list()
  }

  def findByFollower(user: String): List[Player] = DB withSession { implicit session =>
    (for {
      playerIds <- PlayerFollowersTable.q
      players <- q if players.id === playerIds.playerId
    } yield players).list()
  }
}

class PlayerFollowersTable(tag: Tag) extends Table[(Long, String)](tag, "player_followers"){
  def playerId = column[Long]("player_id")
  def user = column[String]("user")
  def * = (playerId, user)
  def player = foreignKey("player_followers_player_fk", playerId, PlayerTable.q)(_.id)
  def pk = primaryKey("player_followers_pk", playerId -> user)
}

object PlayerFollowersTable {
  val q = TableQuery[PlayerFollowersTable]

  def follow(id: Long, user: String) = DB withSession { implicit session =>
    q.insert(id -> user)
  }

  def unfollow(id: Long, user: String) = DB withSession { implicit session =>
    q.where(_.playerId === id).where(_.user === user).delete
  }

  def findByUser(user: String): Seq[Long] = DB withSession { implicit session =>
    q.where(_.user === user).map(_.playerId).list()
  }
}