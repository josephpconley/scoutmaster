package controllers

import controllers.Application.Authenticated
import messages.JSONFormats
import models.{TagFollowersTable, PlayerTable}
import play.api.libs.json._
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


object Tags extends Controller with JSONFormats with MongoController {

  def collection: JSONCollection = db.collection[JSONCollection]("items")

  def index(tagName: Option[String]) = Authenticated { implicit req =>
    Ok(views.html.tags(tagName))
  }

  def search(q: String) = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { items =>
      val tags: Set[String] = items.foldLeft(Set.empty[String]){ (set, item) =>
        set ++ ((item \ "tags").as[JsArray].value map (tag => (tag \ "text").as[String])).toSet
      }

      Ok(Json.toJson(tags filter (tag => tag.toLowerCase().contains(q.toLowerCase()))))
    }
  }

  def list = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.sort(Json.obj("updateDate" -> -1)).cursor[JsObject].collect[List]() map { items =>

      val tags: Seq[(String, Double)] =
        items.foldLeft(Seq.empty[(String, Double)])( (seq, item) =>
          seq ++ ((item \ "tags").as[JsArray].value map (tag => (tag \ "text").as[String] -> (tag \ "value").as[Double]))
        )

      val totals: Seq[(String, Double)] = tags.groupBy(_._1).map(tag => tag._1 -> tag._2.map(_._2).sum).toSeq
      val jsonObjects: Seq[JsObject] = totals sortBy (_._1) map (tag => Json.obj("text" -> tag._1, "value" -> tag._2))
      Ok(JsArray(jsonObjects))
    }
  }

  def items(tagName: String) = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.sort(Json.obj("updateDate" -> -1)).cursor[JsObject].collect[List]() map { items =>
      val taggedItems = items.filter(item => (item \ "tags").as[JsArray].value.exists( tag => (tag \ "text").as[String] == tagName))
      Ok(JsArray(taggedItems))
    }
  }

  def trending = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { items =>
      val trendingTags = items.map(p => (p \ "tags").as[JsArray]).foldLeft(Map.empty[String, Int] withDefaultValue 0){ (m, tags) =>
        tags.value.foldLeft(m){ (m, tag) =>
          val tagText = (tag \ "text").as[String]
          m + (tagText -> (1 + m(tagText)))
        }
      }.toSeq.sortBy(- _._2).take(3).map(tag => Json.obj("text" -> tag._1, "value" -> tag._2))

      Ok(JsArray(trendingTags))
    }
  }

  def follow(tagName: String) = Authenticated { implicit req =>
    TagFollowersTable.follow(tagName, req.user)
    Ok
  }

  def unfollow(tagName: String) = Authenticated { implicit req =>
    TagFollowersTable.unfollow(tagName, req.user)
    Ok
  }
}