package controllers

import apis.API
import controllers.Application.Authenticated
import messages.JSONFormats
import models.{PlayerFollowersTable, PlayerTable}
import notifications.Email
import org.joda.time.DateTime
import play.api.db.slick.DBAction
import play.api.libs.json._
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Players extends Controller with JSONFormats with MongoController {

  def collection: JSONCollection = db.collection[JSONCollection]("items")

  def index(id: Option[Long]) = Authenticated { implicit req =>
    Ok(views.html.players(id))
  }

  def list = DBAction { implicit req =>
    Ok(Json.toJson(PlayerTable.list))
  }

  def search(playerId: Long, apiId: String, name: String) = Authenticated.async { implicit req =>
    API.list find( _.id == apiId) map { api =>
      for {
        items <- collection.find(Json.obj("playerId" -> playerId, "itemId" -> api.itemId)).cursor[JsObject].collect[List]()
        ids = items map (js => api.idReader(js \ "item"))
        searchResults <- api.search(name, ids)
      } yield searchResults

    } getOrElse Future.successful(NotFound)
  }


  lazy val savedKeys = Seq("itemId", "item", "notes", "tags", "playerId", "_id")

  def save = Authenticated.async(parse.json) { implicit req =>
    val body = req.body.as[JsObject]
    val fields = body.value.filter(obj => savedKeys.contains(obj._1)).toSeq
    val item = JsObject(fields) ++ Json.obj("updateDate" -> DateTime.now(), "createdBy" -> req.user)
    collection.save(item).map{ error =>
      error.errMsg map (e => BadRequest(e)) getOrElse {
        if(!body.keys.contains("_id")){
          Email.newPlayerPost(body)
        }
        Ok(item)
      }
    }
  }

  def items(id: Long) = Authenticated.async { implicit req =>
    collection.find(Json.obj("playerId" -> id)).sort(Json.obj("updateDate" -> -1)).cursor[JsObject].collect[List]() map { items =>
      Ok(JsArray(items))
    }
  }

  def trending = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { playerIds =>
      val trendingIds = playerIds.map(p => (p \ "playerId").as[Long]).foldLeft(Map.empty[Long, Int] withDefaultValue 0){ (m, id) =>
        m + (id -> (1 + m(id)))
      }.toSeq.sortBy(- _._2).take(3).map(_._1)

      Ok(Json.toJson(PlayerTable.findByIds(trendingIds)))
    }
  }

  def leaders(tagName: String) = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { items =>
      val tagMap = items.foldLeft(Map.empty[Long, Int] withDefaultValue 0){ (m, item) =>
        val playerId = (item \ "playerId").as[Long]
        val tags = (item \ "tags").as[JsArray].value.filter(tag => (tag \ "text").as[String] == tagName)

        tags.foldLeft(m){ (m, tag) =>
          val tagText = (tag \ "text").as[String]
          m + (playerId -> (1 + m(playerId)))
        }
      }

      val players = PlayerTable.findByIds(tagMap.keySet.toSeq) map { player =>
        Json.obj("player" -> player, "tagCount" -> tagMap(player.id))
      }

      Ok(JsArray(players.sortBy(p => -(p \ "tagCount").as[Long])))
    }
  }

  def rankings = Authenticated { implicit req =>
    Ok(views.html.rankings())
  }

  case class Tag(text: String, value: Double)

  def fetch = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { items =>
      val tagMap = items.foldLeft(Map.empty[Long, Seq[Tag]] withDefaultValue Seq.empty[Tag]){ (m, item) =>
        val playerId = (item \ "playerId").as[Long]
        val tags: Seq[Tag] = (item \ "tags").as[JsArray].value map (t => Json.fromJson[Tag](t).get)

        m + (playerId -> (m.apply(playerId) ++ tags))
      }

      val avgTagMap: Map[Long, Seq[Tag]] = tagMap.map { kv =>
        val averages = kv._2.groupBy(_.text).map( t => t._1 -> (t._2.map(_.value).sum.toDouble / t._2.size))
        kv._1 -> averages.map(t => Tag(t._1, t._2)).toSeq
      }.toMap withDefaultValue Seq.empty[Tag]

      val players = PlayerTable.find map { player =>
        Json.obj("player" -> Json.toJson(player), "tags" -> avgTagMap(player.id))
      }
      Ok(Json.toJson(players))
    }
  }

  def follow(id: Long) = Authenticated { implicit req =>
    PlayerFollowersTable.follow(id, req.user)
    Ok
  }

  def unfollow(id: Long) = Authenticated { implicit req =>
    PlayerFollowersTable.unfollow(id, req.user)
    Ok
  }
}