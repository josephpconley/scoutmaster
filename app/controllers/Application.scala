package controllers

import extensions.Models
import play.api.mvc.Security.AuthenticatedBuilder
import play.api.mvc._

import play.api.data.Forms._
import play.api.data.Form

object Application extends Controller {

  def index = Authenticated { implicit req =>
    Ok(views.html.index())
  }

  val newUserForm = Form(single("user" -> nonEmptyText))

  def login = Action { implicit req =>
    Ok(views.html.login(newUserForm))
  }

  def logout = Action { implicit req =>
    Redirect(routes.Application.index).withNewSession
  }

  def auth = Action { implicit req =>
    newUserForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.login(formWithErrors)),
      user => Redirect(routes.Application.index()).withSession("user" -> user)
    )
  }

  def ddl = Action { implicit req =>
    val ddl = Models.ddl
    Ok(ddl.dropStatements.mkString(";\n") + ";\n" + ddl.createStatements.mkString(";\n") + ";")
  }

  object Authenticated extends AuthenticatedBuilder(
    req => req.session.get("user"),
    req => Redirect(routes.Application.login)
  )
}