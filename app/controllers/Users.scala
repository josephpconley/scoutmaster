package controllers

import controllers.Application.Authenticated
import messages.JSONFormats
import models.{UserNotificationsTable, PlayerTable, PlayerFollowersTable, TagFollowersTable}
import play.api.libs.json._
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection

import scala.concurrent.ExecutionContext.Implicits.global

object Users extends Controller with JSONFormats with MongoController {

  def collection: JSONCollection = db.collection[JSONCollection]("items")

  def feed = Authenticated.async { implicit req =>
    collection.genericQueryBuilder.sort(Json.obj("updateDate" -> -1)).cursor[JsObject].collect[List]() map { items =>
      val players = PlayerFollowersTable.findByUser(req.user)
      val tags = TagFollowersTable.findByUser(req.user)
      val filteredItems = items.filter{ item =>
        players.contains( (item \ "playerId").as[Long]) ||
        tags.intersect( (item \ "tags").as[JsArray].value.map(tag => (tag \ "text").as[String])).nonEmpty
      }

      val finalItems = if(filteredItems.isEmpty) items else filteredItems
      Ok(JsArray(finalItems))
    }
  }

  def profile(user: String) = Authenticated { implicit req =>
    Ok(views.html.users.profile(user, UserNotificationsTable.findByUser(user)))
  }

  def saveEmail(email: String) = Authenticated { implicit req =>
    UserNotificationsTable.save(req.user, email)
    Ok
  }

  def items(user: String) = Authenticated.async { implicit req =>
    collection.find(Json.obj("createdBy" -> user)).sort(Json.obj("updateDate" -> -1)).cursor[JsObject].collect[List]() map { items =>
      Ok(JsArray(items))
    }
  }

  def playerIds(user: Option[String]) = Authenticated { implicit req =>
    Ok(Json.toJson(PlayerFollowersTable.findByUser(user getOrElse req.user)))
  }

  def tagIds(user: Option[String]) = Authenticated { implicit req =>
    Ok(Json.toJson(TagFollowersTable.findByUser(user getOrElse req.user)))
  }

  def players(user: Option[String]) = Authenticated { implicit req =>
    Ok(Json.toJson(PlayerTable.findByFollower(user getOrElse req.user)))
  }

  def tags(user: Option[String]) = Authenticated.async { implicit req =>
    val tags = TagFollowersTable.findByUser(user getOrElse req.user)

    collection.genericQueryBuilder.cursor[JsObject].collect[List]() map { items =>
      val trendingTags = items.map(p => (p \ "tags").as[JsArray]).foldLeft(Map.empty[String, Int] withDefaultValue 0){ (m, tags) =>
        tags.value.foldLeft(m){ (m, tag) =>
          val tagText = (tag \ "text").as[String]
          m + (tagText -> (1 + m(tagText)))
        }
      }.toSeq.filter(t => tags.contains(t._1)).sortBy(- _._2).map(tag => Json.obj("text" -> tag._1, "value" -> tag._2))

      Ok(JsArray(trendingTags))
    }
  }
}