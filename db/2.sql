create table "player_followers" ("player_id" BIGINT NOT NULL,"user" VARCHAR(254) NOT NULL);
alter table "player_followers" add constraint "player_followers_pk" primary key("player_id","user");
alter table "player_followers" add constraint "player_followers_player_fk" foreign key("player_id") references "player"("id") on update NO ACTION on delete NO ACTION;

create table "tag_followers" ("tag" VARCHAR(254) NOT NULL,"user" VARCHAR(254) NOT NULL);
alter table "tag_followers" add constraint "tag_followers_pk" primary key("tag","user");