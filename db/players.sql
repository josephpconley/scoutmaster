--SQL generated from datacombinator.com (http://api.datacombinator.com/joec/nba/draftpicks/insert)
insert into player values ('Jahlil'
, 'Okafor'
, 18
, 83
, 275
, 'C'
, 'Duke'
, 20123
, 1
);
insert into player values ('Emmanuel'
, 'Mudiay'
, 18
, 77
, 196
, 'PG'
, 'China'
, 20127
, 2
);
insert into player values ('Karl-Anthony'
, 'Towns'
, 18
, 85
, 250
, 'C'
, 'Kentucky'
, 20129
, 3
);
insert into player values ('Kristaps'
, 'Porzingis'
, 19
, 83
, 220
, 'PF'
, 'Latvia'
, 20069
, 4
);
insert into player values ('Kelly'
, 'Oubre'
, 18
, 79
, 204
, 'SF'
, 'Kansas'
, 20130
, 5
);
insert into player values ('Stanley'
, 'Johnson'
, 18
, 79
, 237
, 'SF'
, 'Arizona'
, 20131
, 6
);
insert into player values ('Cliff'
, 'Alexander'
, 18
, 80
, 251
, 'PF'
, 'Kansas'
, 20126
, 7
);
insert into player values ('Kevon'
, 'Looney'
, 18
, 82
, 210
, 'SF'
, 'UCLA'
, 20153
, 8
);
insert into player values ('Myles'
, 'Turner'
, 18
, 85
, 240
, 'C'
, 'Texas'
, 20124
, 9
);
insert into player values ('Mario'
, 'Hezonja'
, 19
, 79
, 200
, 'SF'
, 'Croatia'
, 19973
, 10
);
insert into player values ('Terry'
, 'Rozier'
, 20
, 74
, 190
, 'PG'
, 'Louisville'
, 20027
, 11
);
insert into player values ('Chris'
, 'Walker'
, 19
, 81
, 195
, 'SF'
, 'Florida'
, 19988
, 12
);
insert into player values ('Justise'
, 'Winslow'
, 18
, 78
, 222
, 'SF'
, 'Duke'
, 20134
, 13
);
insert into player values ('Willie'
, 'Cauley-Stein'
, 21
, 85
, 220
, 'C'
, 'Kentucky'
, 19853
, 14
);
insert into player values ('Montrezl'
, 'Harrell'
, 20
, 80
, 235
, 'PF'
, 'Louisville'
, 19917
, 15
);
insert into player values ('Sam'
, 'Dekker'
, 20
, 80
, 215
, 'SF'
, 'Wisconsin'
, 19857
, 16
);
insert into player values ('Rondae'
, 'Hollis-Jefferson'
, 19
, 79
, 205
, 'SF'
, 'Arizona'
, 19989
, 17
);
insert into player values ('Justin'
, 'Jackson'
, 19
, 80
, 200
, 'SF'
, 'North Carolina'
, 20135
, 18
);
insert into player values ('Bobby'
, 'Portis'
, 19
, 83
, 231
, 'PF'
, 'Arkansas'
, 19987
, 19
);
insert into player values ('Wayne'
, 'Selden'
, 19
, 77
, 230
, 'SG'
, 'Kansas'
, 19986
, 20
);
insert into player values ('Caris'
, 'LeVert'
, 20
, 78
, 185
, 'SF'
, 'Michigan'
, 20108
, 21
);
insert into player values ('Egemen'
, 'Guven'
, 17
, 81
, 210
, 'PF'
, 'Turkey'
, 20156
, 22
);
insert into player values ('R.J.'
, 'Hunter'
, 20
, 77
, 185
, 'SG'
, 'Georgia St'
, 20152
, 23
);
insert into player values ('Tyus'
, 'Jones'
, 18
, 73
, 170
, 'PG'
, 'Duke'
, 20132
, 24
);
insert into player values ('Ron'
, 'Baker'
, 21
, 75
, 213
, 'PG'
, 'Wichita State'
, 20074
, 25
);
insert into player values ('Ilimane'
, 'Diop'
, 19
, 85
, 230
, 'C'
, 'Senegal'
, 20068
, 26
);
insert into player values ('Marc'
, 'Garcia'
, 18
, 78
, 180
, 'SG'
, 'Spain'
, 20147
, 27
);
insert into player values ('Alex'
, 'Poythress'
, 21
, 79
, 215
, 'SF'
, 'Kentucky'
, 19807
, 28
);
insert into player values ('Jarell'
, 'Martin'
, 20
, 81
, 241
, 'PF'
, 'LSU'
, 19985
, 29
);
insert into player values ('Norman'
, 'Powell'
, 21
, 76
, 215
, 'SG'
, 'UCLA'
, 20154
, 30
);
insert into player values ('Branden'
, 'Dawson'
, 21
, 78
, 220
, 'SF'
, 'Michigan State'
, 19736
, 31
);
insert into player values ('Delon'
, 'Wright'
, 22
, 77
, 178
, 'PG'
, 'Utah'
, 20133
, 32
);
insert into player values ('Buddy'
, 'Hield'
, 20
, 76
, 208
, 'SG'
, 'Oklahoma'
, 20148
, 33
);
insert into player values ('Aaron'
, 'Harrison'
, 19
, 77
, 210
, 'SG'
, 'Kentucky'
, 19906
, 34
);
insert into player values ('Dakari'
, 'Johnson'
, 19
, 82
, 250
, 'C'
, 'Kentucky'
, 19970
, 35
);
insert into player values ('Rashad'
, 'Vaughn'
, 18
, 77
, 200
, 'SG'
, 'UNLV'
, 20157
, 36
);
insert into player values ('Marcus'
, 'Paige'
, 21
, 73
, 157
, 'PG'
, 'North Carolina'
, 19862
, 37
);
insert into player values ('Andrew'
, 'Harrison'
, 19
, 77
, 207
, 'PG'
, 'Kentucky'
, 19901
, 38
);
insert into player values ('Frank'
, 'Kaminsky'
, 21
, 85
, 234
, 'C'
, 'Wisconsin'
, 20080
, 39
);
insert into player values ('Jabari'
, 'Bird'
, 20
, 78
, 190
, 'SG'
, 'California'
, 19993
, 40
);
insert into player values ('Brice'
, 'Johnson'
, 20
, 81
, 187
, 'PF'
, 'North Carolina'
, 19921
, 41
);
insert into player values ('Amida'
, 'Brimah'
, 20
, 85
, 217
, 'C'
, 'Connecticut'
, 20155
, 42
);
insert into player values ('Terran'
, 'Petteway'
, 21
, 78
, 209
, 'SG'
, 'Nebraska'
, 20114
, 43
);
insert into player values ('Trey'
, 'Lyles'
, 18
, 82
, 230
, 'PF'
, 'Kentucky'
, 20128
, 44
);
insert into player values ('Jordan'
, 'Mickey'
, 20
, 80
, 235
, 'PF'
, 'LSU'
, 20075
, 45
);
insert into player values ('Theo'
, 'Pinson'
, 18
, 78
, 190
, 'SF'
, 'North Carolina'
, 20164
, 46
);
insert into player values ('Marcus'
, 'Lee'
, 20
, 81
, 200
, 'PF'
, 'Kentucky'
, 19971
, 47
);
insert into player values ('Marko'
, 'Arapovic'
, 18
, 82
, 230
, 'C'
, 'Croatia'
, 20162
, 48
);
insert into player values ('Kasey'
, 'Hill'
, 20
, 73
, 175
, 'PG'
, 'Florida'
, 19972
, 49
);
insert into player values ('Brandon'
, 'Ashley'
, 20
, 80
, 230
, 'SF'
, 'Arizona'
, 19855
, 50
);
insert into player values ('A.J.'
, 'Hammons'
, 22
, 85
, 280
, 'C'
, 'Purdue'
, 20018
, 51
);
insert into player values ('Damian'
, 'Jones'
, 19
, 82
, 235
, 'C'
, 'Vanderbilt'
, 20180
, 52
);
insert into player values ('Isaiah'
, 'Taylor'
, 20
, 75
, 170
, 'PG'
, 'Texas'
, 20185
, 53
);
insert into player values ('Moussa'
, 'Diagne'
, 20
, 83
, 230
, 'C'
, 'Senegal'
, 20138
, 54
);
insert into player values ('Kenneth'
, 'Smith'
, 21
, 75
, 180
, 'PG'
, 'Louisiana Tech'
, 20122
, 55
);
insert into player values ('Shawn'
, 'Long'
, 21
, 81
, 245
, 'PF'
, 'Louisiana Lafayette'
, 20117
, 56
);
insert into player values ('Kenan'
, 'Sipahi'
, 19
, 76
, 180
, 'PG'
, 'Kosovo'
, 20066
, 57
);
insert into player values ('Sindarius'
, 'Thornwell'
, 19
, 77
, 205
, 'SG'
, 'South Carolina'
, 20078
, 58
);
insert into player values ('D''Angelo'
, 'Russell'
, 18
, 77
, 176
, 'SG'
, 'Ohio State'
, 20178
, 59
);
insert into player values ('Devin'
, 'Booker'
, 17
, 78
, 195
, 'SG'
, 'Kentucky'
, 20159
, 60
);
insert into player values ('Nedim'
, 'Buza'
, 19
, 79
, 190
, 'SF'
, 'Bosnia'
, 20140
, 61
);
insert into player values ('Juwan'
, 'Staten'
, 22
, 73
, 190
, 'PG'
, 'West Virginia'
, 20099
, 62
);
insert into player values ('Kaleb'
, 'Tarczewski'
, 21
, 85
, 230
, 'C'
, 'Arizona'
, 19798
, 63
);
insert into player values ('Rasmus'
, 'Larsen'
, 19
, 83
, 225
, 'PF'
, 'Denmark'
, 19866
, 64
);
insert into player values ('Jerian'
, 'Grant'
, 21
, 77
, 185
, 'PG'
, 'Notre Dame'
, 19856
, 65
);
insert into player values ('Yankuba'
, 'Sima'
, 18
, 82
, 215
, 'C'
, 'Spain'
, 20176
, 66
);
insert into player values ('Jamari'
, 'Traylor'
, 22
, 80
, 220
, 'PF'
, 'Kansas'
, 19877
, 67
);
insert into player values ('Michael'
, 'Frazier'
, 20
, 76
, 200
, 'SG'
, 'Florida'
, 20029
, 68
);
insert into player values ('Michael'
, 'Qualls'
, 20
, 78
, 210
, 'SG'
, 'Arkansas'
, 20092
, 69
);
insert into player values ('Guillem'
, 'Vives'
, 21
, 76
, 180
, 'PG'
, 'Spain'
, 20137
, 70
);
insert into player values ('Devin'
, 'Robinson'
, 19
, 80
, 180
, 'SF'
, 'Florida'
, 20169
, 71
);
insert into player values ('Marcus'
, 'Foster'
, 19
, 74
, 200
, 'PG'
, 'Kansas State'
, 20101
, 72
);
insert into player values ('Isaac'
, 'Copeland'
, 19
, 82
, 210
, 'SF'
, 'Georgetown'
, 20179
, 73
);
insert into player values ('Domantas'
, 'Sabonis'
, 18
, 82
, 230
, 'PF'
, 'Gonzaga'
, 20160
, 74
);
insert into player values ('Rasheed'
, 'Sulaimon'
, 20
, 76
, 190
, 'SG'
, 'Duke'
, 19808
, 75
);
insert into player values ('Isaac'
, 'Hamilton'
, 20
, 77
, 185
, 'SG'
, 'UCLA'
, 20020
, 76
);
insert into player values ('Treveon'
, 'Graham'
, 20
, 77
, 215
, 'SG'
, 'VA Commonwealth'
, 19910
, 77
);
insert into player values ('Chris'
, 'Obekpa'
, 20
, 81
, 223
, 'PF'
, 'St. John''s'
, 19942
, 78
);
insert into player values ('Tyler'
, 'Ulis'
, 18
, 69
, 142
, 'PG'
, 'Kentucky'
, 20172
, 79
);
insert into player values ('Chris'
, 'McCullough'
, 19
, 81
, 200
, 'PF'
, 'Syracuse'
, 20163
, 80
);
insert into player values ('Timothe'
, 'Luwawu'
, 19
, 78
, 205
, 'SF'
, 'France'
, 20181
, 81
);
insert into player values ('Troy'
, 'Williams'
, 19
, 79
, 206
, 'SF'
, 'Indiana'
, 20081
, 82
);
insert into player values ('E.C.'
, 'Matthews'
, 18
, 76
, 180
, 'SG'
, 'Rhode Island'
, 20158
, 83
);
insert into player values ('Tony'
, 'Parker'
, 21
, 81
, 255
, 'C'
, 'UCLA'
, 19882
, 84
);
insert into player values ('Rodney'
, 'Purvis'
, 20
, 74
, 195
, 'SG'
, 'Connecticut'
, 19861
, 85
);
insert into player values ('Zak'
, 'Irvin'
, 20
, 78
, 200
, 'SG'
, 'Michigan'
, 20022
, 86
);
insert into player values ('Nikola'
, 'Milutinov'
, 19
, 83
, 220
, 'C'
, 'Serbia'
, 20039
, 87
);
insert into player values ('Dyshawn'
, 'Pierre'
, 20
, 78
, 210
, 'SF'
, 'Dayton'
, 20085
, 88
);
insert into player values ('Joseph'
, 'Young'
, 22
, 75
, 185
, 'SG'
, 'Oregon'
, 20077
, 89
);
insert into player values ('Daniel'
, 'Hamilton'
, 19
, 81
, 178
, 'SF'
, 'Connecticut'
, 20186
, 90
);
insert into player values ('Andzejs'
, 'Pasecniks'
, 18
, 85
, 200
, 'C'
, 'Latvia'
, 20175
, 91
);
insert into player values ('Tyler'
, 'Haws'
, 23
, 77
, 200
, 'SG'
, 'BYU'
, 19920
, 92
);
insert into player values ('Austin'
, 'Nichols'
, 20
, 81
, 212
, 'PF'
, 'Memphis'
, 20051
, 93
);
insert into player values ('Nigel'
, 'Williams-Goss'
, 19
, 75
, 180
, 'PG'
, 'Washington'
, 20001
, 94
);
insert into player values ('Anthony'
, 'Barber'
, 20
, 74
, 165
, 'PG'
, 'NC State'
, 19992
, 95
);
insert into player values ('Mamadou'
, 'Ndiaye'
, 21
, 90
, 290
, 'C'
, 'UC Irvine'
, 20076
, 96
);
insert into player values ('Mangok'
, 'Mathiang'
, 21
, 82
, 220
, 'C'
, 'Louisville'
, 20168
, 97
);
insert into player values ('Le''Bryan'
, 'Nash'
, 22
, 79
, 230
, 'SF'
, 'Oklahoma State'
, 19657
, 98
);
insert into player values ('Boris'
, 'Dallo'
, 20
, 76
, 180
, 'PG'
, 'France'
, 19915
, 99
);
insert into player values ('Olivier'
, 'Hanlan'
, 21
, 76
, 188
, 'SG'
, 'Boston College'
, 20043
, 100
);
