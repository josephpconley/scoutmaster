create table "user_notifications" (
    "user" VARCHAR(254) NOT NULL,
    "email" VARCHAR(254) NOT NULL,
    primary key ("user", email)
);