name := "scoutmaster"

version := "1.0"

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  cache,
  ws,
  "com.typesafe" %% "play-plugins-mailer" % "2.2.0",
  //db
  "com.typesafe.play" %% "play-slick" % "0.7.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.10.5.akka23-SNAPSHOT",
  //webjars
  "org.webjars" % "bootstrap" % "3.1.1",
  "org.webjars" % "font-awesome" % "4.2.0",
  "org.webjars" % "knockout" % "3.0.0",
  "org.webjars" % "knockout-projections" % "1.0.0",
  "org.webjars" % "tablesorter" % "2.15.5",
  "org.webjars" % "flot" % "0.8.3",
  "org.webjars" % "typeaheadjs" % "0.10.4-1"
)

lazy val scoutmaster = (project in file(".")).enablePlugins(PlayScala)